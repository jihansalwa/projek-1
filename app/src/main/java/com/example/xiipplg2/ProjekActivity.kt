package com.example.xiipplg2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class ProjekActivity : AppCompatActivity() {

    lateinit var projekView: RecyclerView
    lateinit var projekAdapter: ProjekAdapter
    val list = ArrayList<ProjekData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_projek)

        projekView = findViewById(R.id.rvProjek)
        projekView.layoutManager = LinearLayoutManager( this)

        list.add(ProjekData("Lks Web", "Projek latihan Lks Web",
            "https://gitlab.com/jihansalwa/profil-guru-jihan-salwa"))

        list.add(
            ProjekData("Kasir Depot", "Aplikasi kasir depot air",
                "https://gitlab.com/jihansalwa/jobsheet192")
        )

        projekAdapter = ProjekAdapter(list)
        projekView.adapter = projekAdapter
    }
}