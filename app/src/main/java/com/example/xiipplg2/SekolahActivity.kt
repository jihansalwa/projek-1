package com.example.xiipplg2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class SekolahActivity : AppCompatActivity() {
    lateinit var sekolahView: RecyclerView
    lateinit var sekolahAdapter: SekolahAdapter
    val list = ArrayList<SekolahData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sekolah)

        sekolahView = findViewById(R.id.rvSekolah)
        sekolahView.layoutManager = LinearLayoutManager( this)

        list.add(SekolahData("SD Tangkis","Tangkis,kec Guntur,kab Demak"))
        list.add(SekolahData("MTS Gaji","Jl.Karang No.2, RT.01/RW.02,Krajan,Gaji,kec.Guntur,kab.Demak"))
        list.add(SekolahData("SMKN 1 Sayung","Jl.Raya Semarang Demak Km 14 Onggorawe Sayung Demak"))

        sekolahAdapter = SekolahAdapter(list)
        sekolahView.adapter = sekolahAdapter
    }
}